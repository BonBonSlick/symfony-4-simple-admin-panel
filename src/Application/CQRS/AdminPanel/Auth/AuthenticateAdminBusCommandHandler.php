<?php
declare(strict_types = 1);

namespace App\Application\CQRS\AdminPanel\Auth;

class AuthenticateAdminBusCommandHandler
{
    /**
     * @param AuthenticateAdminBusCommand $command
     */
    public function handle(AuthenticateAdminBusCommand $command) : void
    {
        dump('LOGGED!');
        dump($command);
        exit;
    }
}