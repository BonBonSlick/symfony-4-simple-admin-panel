<?php
declare(strict_types = 1);

namespace App\Application\CQRS\AdminPanel\User;

use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;

class GetUserListHandler
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * GetUserListHandler constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUserList $command
     *
     * @return User[]|mixed
     */
    public function handle(GetUserList $command)
    {
        return $this->userRepository->all($command->getFilter());
    }
}