<?php
declare(strict_types = 1);

namespace App\Application\CQRS\AdminPanel\User;

use App\Domain\User\Filter\UserFilter;

class GetUserList
{
    /**
     * @var UserFilter
     */
    private $filter;

    /**
     * GetUserList constructor.
     *
     * @param UserFilter $filter
     */
    public function __construct(UserFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return UserFilter
     */
    public function getFilter() : UserFilter
    {
        return $this->filter;
    }
}