<?php
declare(strict_types = 1);

namespace App\Infrastructure\Controller\AdminPanel;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends  AdminBaseController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request) : Response
    {
        return $this->render('admin_area/dashboard.html.twig', [
            //'data' => $data,
        ]);
    }
}