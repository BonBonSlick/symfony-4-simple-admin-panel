<?php
declare(strict_types = 1);

namespace App\Infrastructure\Controller\AdminPanel;

use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminBaseController extends Controller
{
    /**
     * @var CommandBus
     */
    public $commandBus;

    /**
     * FrontBaseController constructor.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }
}