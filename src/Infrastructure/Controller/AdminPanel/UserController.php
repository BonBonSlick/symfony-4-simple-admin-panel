<?php
declare(strict_types = 1);

namespace App\Infrastructure\Controller\AdminPanel;

use App\Application\CQRS\AdminPanel\User\GetUserList;
use App\Domain\User\Filter\UserFilter;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AdminBaseController
{
    /**
     * @return Response
     */
    public function index() : Response
    {
        return $this->render('admin_area/user/index.html.twig', [
            'users' => $this->commandBus->handle(new GetUserList(new UserFilter())),
        ]);
    }
}