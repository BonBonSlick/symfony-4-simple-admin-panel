<?php
declare(strict_types = 1);

namespace App\Infrastructure\Controller\AdminPanel\Auth;

use App\Application\CQRS\AdminPanel\Auth\AuthenticateAdminBusCommand;
use App\Domain\Core\Exceptions\FormValidationException;
use App\Infrastructure\Controller\AdminPanel\AdminBaseController;
use App\Infrastructure\Validation\Request\AdminPanel\Auth\AdminLoginValidationRequest;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LogInController extends AdminBaseController
{
    /**
     * @return Response
     */
    public function fakeLoginPage() : Response
    {
        return $this->render('admin_area/auth/fake_login.html.twig');
    }
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function loginPage(Request $request) : Response
    {
        return $this->render('admin_area/auth/login.html.twig', [
            'message' => $request->query->get('message') ? : '',
            'violations' => $request->query->get('violations') ? : '',
        ]);
    }

    /**
     * @param Request                     $request
     * @param AdminLoginValidationRequest $validator
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function authenticate(
        Request $request,
        AdminLoginValidationRequest $validator) : Response
    {

        dump($this->commandBus);

        $this->commandBus->handle(
            new AuthenticateAdminBusCommand(
                $request->request->get('email'),
                $request->request->get('password')
            )
        );
        exit;

        //AuthenticationUtils $authenticationUtils
        // get the login error if there is one
        //$error = $authenticationUtils->getLastAuthenticationError();
        //$lastUsername = $authenticationUtils->getLastUsername();


        //try {
        //    $validator->validate($request);
        //} catch (FormValidationException $exception) {
        //    return $this->render(
        //        'admin_area/auth/login.html.twig',
        //        [
        //            'message'    => $exception->getMessage(),
        //            'violations' => $exception->getViolations(),
        //            'old'        => $exception->getOldInputData(),
        //        ]
        //    );
        //}

        //return new Response('123');

        //return new RedirectResponse($this->generateUrl('admin.dashboard'));
    }
}