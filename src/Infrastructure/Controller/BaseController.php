<?php
declare(strict_types = 1);

namespace App\Infrastructure\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
}