<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\Doctrine\DataFixtures;

use App\Domain\User\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserFixtures extends Fixture
{
    public  $encoderFactory;

    /**
     * UserFixtures constructor.
     *
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws \RuntimeException
     */
    public function load(ObjectManager $manager) : void
    {
        $admin = User::register( 'admin@email.com', 'admin');
        $admin->changeSalt('some_rand_string');
        $encodedPass = $this->encoderFactory->getEncoder($admin)->encodePassword(
            $admin->toPlainPassword(),
            $admin->getSalt()
        );
        $admin->changePassword($encodedPass);
        $admin->eraseCredentials();
        $manager->persist($admin);
        $manager->flush();
    }
}