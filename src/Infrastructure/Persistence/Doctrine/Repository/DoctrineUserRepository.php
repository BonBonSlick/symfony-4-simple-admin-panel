<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\Doctrine\Repository;

use App\Domain\Core\Query\QueryPagination;
use App\Domain\User\Entity\User;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Filter\UserFilter;
use App\Domain\User\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

//final class DoctrineUserRepository extends EntityRepository implements UserRepositoryInterface, UserLoaderInterface
final class DoctrineUserRepository  implements UserRepositoryInterface
{
    /**
     * Alias for Class table
     *
     * @var string
     */
    private $thisRepositoryAlias = 'usersTableAlias';

    /**
     * Class for current repository
     *
     * @var string
     */
    private $repositoryClass = User::class;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * DoctrineUserRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function all(
        UserFilter $filter,
        QueryPagination $pagination = null
    ) {
        try {
            $queryBuilder = $this->createQueryBuilder();
            $queryBuilder = $this->filter($queryBuilder, $filter);
            if ($pagination) {
                $queryBuilder
                    ->setFirstResult($pagination->offset())
                    ->setMaxResults($pagination->limit());
            }

            return $queryBuilder
                ->getQuery()
                ->getResult();
        } catch (NoResultException $exception) {
            return [];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function single(
        UserFilter $filter
    ) : ? User
    {
        try {
            $queryBuilder = $this->createQueryBuilder();
            $queryBuilder = $this->filter($queryBuilder, $filter);

            return $queryBuilder
                ->getQuery()
                ->getSingleResult();
        } catch (NonUniqueResultException $exception) {
            throw new UserNotFoundException($exception->getMessage());
        } catch (NoResultException $exception) {
            throw new UserNotFoundException($exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function save(User $entity) : void
    {
        //$this->_em->persist($entity);
        //$this->_em->clear($entity);
        $this->entityManager->persist($entity);
        $this->entityManager->clear($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(User $entity) : void
    {
        $this->entityManager->remove($entity);
        $this->entityManager->clear($entity);
    }

    /**
     *
     *
     * THIS METHOD SHOULD BE THE LAST ONE!
     *
     *
     * Sets filter parameters for query
     *
     * @param QueryBuilder $queryBuilder
     * @param UserFilter   $filter
     *
     * @return QueryBuilder
     */
    private function filter(QueryBuilder $queryBuilder, UserFilter $filter) : QueryBuilder
    {
        if ($filter->toEmail()) {
            $queryBuilder
                ->andWhere($this->thisRepositoryAlias . '.email = :email')
                ->setParameter('email', $filter->toEmail());
        }

        return $queryBuilder;
    }

    /**
     * Loads the user for the given username.
     * Used in Symfony Security services, authentication and authorization.
     *
     * This method must return null if the user is not found.
     *
     * @param string $string
     *
     * @return User|null
     * @throws UserNotFoundException
     */
    public function loadUserByUsername($string) : ? User
    {
        try {
            return $this->createQueryBuilder()
                ->where($this->thisRepositoryAlias . '.apiKey = :key')
                ->orWhere($this->thisRepositoryAlias . '.email = :key')
                ->setParameter('key', $string)
                ->getQuery()
                ->getSingleResult();
        } catch (NonUniqueResultException $exception) {
            throw new UserNotFoundException($exception->getMessage());
        } catch (NoResultException $exception) {
            throw new UserNotFoundException($exception->getMessage());
        }
    }


    /**
     * Creates a new QueryBuilder instance that is pre populated for this entity name.
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select($this->thisRepositoryAlias)
            ->from($this->repositoryClass, $this->thisRepositoryAlias)
            ;
    }
}