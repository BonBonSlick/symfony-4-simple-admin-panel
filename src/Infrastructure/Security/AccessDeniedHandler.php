<?php

namespace App\Infrastructure\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * @param Request $request
     * @param AccessDeniedException $accessDeniedException

     * @return JsonResponse|AccessDeniedHttpException
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        // Check if url exist api string
        if (strpos($request->getRequestUri(), '/api/') !== false) {
            return new JsonResponse(['error' => $accessDeniedException->getMessage()], $accessDeniedException->getCode());
        }

        return new AccessDeniedHttpException();
    }
}