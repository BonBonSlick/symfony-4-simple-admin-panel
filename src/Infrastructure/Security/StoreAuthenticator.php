<?php

namespace App\Infrastructure\Security;

use App\Domain\Core\Exceptions\AdminAuthenticationException;
use App\Domain\User\Entity\User;
use App\Infrastructure\Security\Authentication\Token\UsernamePasswordStoreToken;
use App\Infrastructure\Validation\Request\AdminPanel\Auth\AdminLoginValidationRequest;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;

class StoreAuthenticator implements SimpleFormAuthenticatorInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface                            $tokenStorage
     * @param SessionInterface                       $session
     * @param EntityManagerInterface                 $entityManager
     * @param UserPasswordEncoderInterface           $encoder
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        SessionInterface $session,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->em = $entityManager;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param TokenInterface        $token
     * @param UserProviderInterface $userProvider
     * @param                       $providerKey
     *
     * @return UsernamePasswordStoreToken
     * @throws \App\Domain\Core\Exceptions\AdminAuthenticationException
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws AuthenticationException
     */
    public function authenticateToken(
        TokenInterface $token,
        UserProviderInterface $userProvider,
        $providerKey) : ?UsernamePasswordStoreToken
    {
        try {
            $user = $userProvider->loadUserByUsername($token->getUsername());
        } catch (UsernameNotFoundException $e) {
            //throw new AdminAuthenticationException('Invalid Email');
            throw new AuthenticationException('');
        }
        if (null === $token->getCredentials()) {
            //throw new AdminAuthenticationException('Invalid Password');
            throw new AuthenticationException('');
        }

        dump($user);

        //$isPasswordValid = $this->encoder->isPasswordValid($user, $token->getCredentials());
        $isPasswordValid = $this->encoder->isPasswordValid($user, 'admin');
        dump($isPasswordValid);
        exit;

        if ($passwordValid === false) {
            //throw new AdminAuthenticationException('Invalid Password');
            throw new AuthenticationException('');
        }
        $token->eraseCredentials();
        //dump(123);
        //exit;
        return new UsernamePasswordToken(
            $user,
            $user->getPassword(),
            $providerKey,
            $user->getRoles()
        );
        //return new UsernamePasswordStoreToken(
        //    $user,
        //    $user->getPassword(),
        //    $providerKey,
        //    $user->getRoles()
        //);
    }

    /**
     * @param TokenInterface $token
     * @param                $providerKey
     *
     * @return bool
     */
    public function supportsToken(TokenInterface $token, $providerKey) : bool
    {
        return $token instanceof UsernamePasswordToken
               && $token->getProviderKey() === $providerKey;
    }

    /**
     * @param Request $request
     * @param         $username
     * @param         $password
     * @param         $providerKey
     *
     * @return UsernamePasswordToken
     * @throws \InvalidArgumentException
     */
    public function createToken(Request $request, $username, $password, $providerKey) : UsernamePasswordToken
    {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }
}