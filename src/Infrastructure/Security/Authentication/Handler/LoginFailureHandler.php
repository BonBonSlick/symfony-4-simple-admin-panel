<?php

namespace App\Infrastructure\Security\Authentication\Handler;

use CommonBundle\Service\ActionLogger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class LoginFailureHandler implements AuthenticationFailureHandlerInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * This is called when an interactive authentication attempt fails. This is
     * called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response The response to return, never null
     * @throws \InvalidArgumentException
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) : Response
    {
        if (!$request->getSession()) {
            return new RedirectResponse($this->router->generate('admin.login', [
                'message'    => $exception->getMessage(),
            ]));
        }
        $e = $request->getSession()->get(Security::AUTHENTICATION_ERROR . '_ex');
        if ($e) {
            $request->getSession()->remove(Security::AUTHENTICATION_ERROR . '_ex');
        }
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, !$e ? $exception : $e);

        return new RedirectResponse($this->router->generate('admin.login', [
            'message'    => $exception->getMessage(),
        ]));
    }
}