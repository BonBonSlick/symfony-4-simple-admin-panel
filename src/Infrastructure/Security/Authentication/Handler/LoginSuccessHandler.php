<?php

namespace App\Infrastructure\Security\Authentication\Handler;

use CommonBundle\Service\ActionLogger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $checker;

    /**
     * @param RouterInterface $router
     * @param AuthorizationCheckerInterface $checker
     */
    public function __construct(RouterInterface $router, AuthorizationCheckerInterface $checker)
    {
        $this->router = $router;
        $this->checker = $checker;
    }

    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return Response never null
     * @throws \InvalidArgumentException
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token) : Response
    {
        return new RedirectResponse($this->router->generate('admin.dashboard'));
    }
}