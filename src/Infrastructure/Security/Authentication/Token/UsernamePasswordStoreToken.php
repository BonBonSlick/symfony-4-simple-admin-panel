<?php

namespace App\Infrastructure\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class UsernamePasswordStoreToken
 * @package App\Infrastructure\Security\Authentication\Token
 */
class UsernamePasswordStoreToken extends UsernamePasswordToken
{
    /**
     * @var int
     */
    private $storeId = 0;

    /**
     * @param $id
     */
    public function setStoreId($id)
    {
        $this->storeId = (int)$id;
    }

    /**
     * @return int
     */
    public function getStoreId() : int
    {
        return $this->storeId;
    }
}