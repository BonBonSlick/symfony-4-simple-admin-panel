<?php
declare(strict_types = 1);

namespace App\Infrastructure\Validation\Request\AdminPanel\Auth;

use App\Domain\Core\Exceptions\FormValidationException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Exception\MissingOptionsException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminLoginValidationRequest
{
    /**
     * @var ValidatorInterface
     */
    public $validator;

    /**
     * AbstractValidationRequest constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(
        ValidatorInterface $validator
    ) {
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     *
     * @return bool|RedirectResponse
     * @throws \App\Domain\Core\Exceptions\FormValidationException
     * @throws \InvalidArgumentException
     */
    public function validate(Request $request)
    {
        try {
            $constraint = new Collection(
                [
                    'email'    => [
                        new Required(),
                        new NotBlank(),
                        new NotNull(),
                        new Email(),
                    ],
                    'password' => [
                        new Required(),
                        new NotBlank(),
                        new NotNull(),
                    ],
                ]
            );
            $violations = $this->getErrorMessages($this->validator->validate($request->request->all(), $constraint));
            if (\count($violations) > 0) {
                throw new FormValidationException(
                    'Validation Error',
                    $violations,
                    $request->request->all()
                );
            }
        } catch (RouteNotFoundException $exception) {
            throw new FormValidationException($exception->getMessage());
        } catch (MissingMandatoryParametersException $exception) {
            throw new FormValidationException($exception->getMessage());
        } catch (InvalidParameterException $exception) {
            throw new FormValidationException($exception->getMessage());
        } catch (MissingOptionsException $exception) {
            throw new FormValidationException($exception->getMessage());
        } catch (InvalidOptionsException $exception) {
            throw new FormValidationException($exception->getMessage());
        } catch (ConstraintDefinitionException $exception) {
            throw new FormValidationException($exception->getMessage());
        }

        return true;
    }

    /**
     * return simple array of error messages
     *
     * @param ConstraintViolationListInterface $errorsCollection
     *
     * @return array
     */
    public function getErrorMessages(ConstraintViolationListInterface $errorsCollection) : array
    {
        $errors = [];
        foreach ($errorsCollection as $error) {
            $errors[trim($error->getPropertyPath(), '[]')] = $error->getMessage();
        }

        return $errors;
    }
}