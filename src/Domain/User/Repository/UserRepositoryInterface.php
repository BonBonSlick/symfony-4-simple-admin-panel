<?php declare(strict_types = 1);

namespace App\Domain\User\Repository;

use App\Domain\Core\Query\QueryOrder;
use App\Domain\Core\Query\QueryPagination;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Filter\UserFilter;
use App\Domain\User\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface UserRepositoryInterface
 * @package App\Domain\User\Repository
 */
interface UserRepositoryInterface
{
    /**
     * @param UserFilter           $filter
     * @param QueryPagination|null $pagination
     *
     * @return User[]|mixed
     */
    public function all(
        UserFilter $filter,
        QueryPagination $pagination = null);

    /**
     * @param UserFilter $filter
     *
     * @return User
     * @throws UserNotFoundException
     */
    public function single(UserFilter $filter) : ? User;

    /**
     * @param User $user
     *
     * @return void
     */
    public function save(User $user) : void;

    /**
     * @param User $user
     *
     * @return void
     */
    public function delete(User $user) : void;
}
