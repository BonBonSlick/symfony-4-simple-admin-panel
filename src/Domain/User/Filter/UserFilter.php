<?php declare(strict_types = 1);

namespace App\Domain\User\Filter;

use App\Domain\User\ValueObject\UserEmail;

/**
 * Class UserFilter
 * @package App\Domain\User\User\Filter
 */
class UserFilter
{
    /**
     * @var UserEmail
     */
    private $email;

    /**
     * @return UserEmail
     */
    public function toEmail() : ? UserEmail
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email) : self
    {
        $this->email = new UserEmail($email);

        return $this;
    }
}