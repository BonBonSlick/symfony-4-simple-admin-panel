<?php
declare(strict_types = 1);

namespace App\Domain\User\ValueObject;

class UserRememberToken
{
    /**
     * @var string
     */
    private $value;

    /**
     * UserRememberToken constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->value;
    }

    /**
     * Checks if passed value equal with current Value Object
     *
     * @param UserRememberToken $valueObject
     *
     * @return bool
     */
    public function equals(UserRememberToken $valueObject) : bool
    {
        return $this->value === $valueObject->getValue();
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }
}