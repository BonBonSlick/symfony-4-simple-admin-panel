<?php
declare(strict_types = 1);

namespace App\Domain\User\ValueObject;

use Ramsey\Uuid\UuidInterface;

class UserID
{
    /**
     * @var string
     */
    private $value;

    /**
     * UserID constructor.
     *
     * @param UuidInterface $value
     */
    public function __construct(UuidInterface $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->value->toString();
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }
}