<?php
declare(strict_types = 1);

namespace App\Domain\User\ValueObject;

use App\Domain\Core\Interfaces\ValueObjectInterface;
use App\Domain\Core\ValidationException;
use App\Infrastructure\Service\VendorPackages\Assert\DataAssert;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserEmail
 * @package App\Domain\User\ValueObject
 */
class UserEmail
{
    /**
     * @var string
     */
    private $value;

    /**
     * UserEmail constructor.
     *
     * @param string $value
     *
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->value;
    }

    /**
     * Checks if passed value equal with current Value Object
     *
     * @param UserEmail $valueObject
     *
     * @return bool
     */
    public function equals(UserEmail $valueObject) : bool
    {
        return $this->value === $valueObject->getValue();
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }
}