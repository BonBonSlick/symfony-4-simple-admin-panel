<?php declare(strict_types = 1);

namespace App\Domain\User\Entity;

use App\Domain\User\ValueObject\UserEmail;
use App\Domain\User\ValueObject\UserID;
use App\Domain\User\ValueObject\UserPlainPassword;
use App\Domain\User\ValueObject\UserRememberToken;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Persistence\Doctrine\Repository\DoctrineUserRepository")
 * @ORM\Table(name = "`user`", indexes = {@ORM\Index(name = "user_search_idx", columns = {"uuid"})})
 *
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var UserId|string
     *
     * @ORM\Id
     * @ORM\Column(type="guid", unique=true)
     */
    private $uuid;

    /**
     * @var UserEmail
     *
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * Salt for encrypted password
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $salt;

    /**
     * Remember me token
     *
     * @var UserRememberToken|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $rememberToken;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var UserPlainPassword|null
     */
    protected $plainPassword;

    /**
     * @var Carbon
     *
     * @ORM\Column(type="datetimetz", nullable=false)
     */
    private $createdAt;

    /**
     * User constructor.
     *
     * @param string $email
     * @param string $password
     */
    private function __construct(
        $email,
        $password
    ) {
        $this->uuid = Uuid::uuid4();
        $this->email = $email;
        $this->password = $password;
        $this->createdAt = new Carbon();
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return self
     */
    public static function register(string $email, string $password) : self
    {
        return new self($email, $password);
    }

    /**
     * @return string
     */
    public function toUuid() : string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function toEmail() : string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function changeEmail(string $email) : self
    {
        $this->email = new UserEmail($email);

        return $this;
    }

    /**
     * @return string
     */
    public function toPassword() : string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function changePassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * sets Plain password to NULL
     */
    public function eraseCredentials() : void
    {
        $this->plainPassword = null;
    }

    /**
     //* @return Carbon
     //* @return DateTime TODO - WHY???
     */
    //public function toCreatedAt() : Carbon
    public function toCreatedAt()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public function serialize() : string
    {
        return serialize(
            [
                $this->uuid,
            ]
        );
    }

    /**
     * @param $serialized
     */
    public function unserialize($serialized) : void
    {
        [
            $this->uuid,
        ] = unserialize($serialized, self::class);
    }

    /**
     * @return UserPlainPassword|null
     */
    public function toPlainPassword() : ? UserPlainPassword
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     *
     * @return User
     */
    public function changePlainPassword(string $plainPassword) : self
    {
        $this->plainPassword = new UserPlainPassword($plainPassword);

        return $this;
    }

    /**
     * @param string $salt
     *
     * @return self
     */
    public function changeSalt(string $salt) : self
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return UserRememberToken|null
     */
    public function toRememberToken() : ? UserRememberToken
    {
        return $this->rememberToken;
    }

    /**
     * @param string $rememberToken
     *
     * @return User
     */
    public function changeRememberToken(string $rememberToken) : self
    {
        $this->rememberToken = new UserRememberToken($rememberToken);

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRoles() : array
    {
        return [
            'ROLE_USER',
        ];
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string
     */
    public function getSalt() : string
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername() : string
    {
        return $this->email;
    }
}
