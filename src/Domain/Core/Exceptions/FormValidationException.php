<?php
declare(strict_types = 1);

namespace App\Domain\Core\Exceptions;

use Exception;

class FormValidationException extends Exception
{
    /**
     * old form input data
     * 
     * @var mixed[]|null
     */
    private $old;

    /**
     * @var string[]||null
     */
    private $violations;

    /**
     * Create a new exception instance.
     *
     * @param string   $message
     * @param string[] $violations
     * @param mixed[]  $old
     */
    public function __construct(
        string $message,
        array $violations = null,
        array $old = null
    ) {
        parent::__construct($message);
        $this->old = $old;
        $this->violations = $violations;
    }

    /**
     * @return  mixed[]
     */
    public function getOldInputData() : ? array
    {
        return $this->old;
    }

    /**
     * @return array|string[]
     */
    public function getViolations() : array
    {
        return $this->violations;
    }
}