<?php
declare(strict_types = 1);

namespace App\Domain\Core\Exceptions;

class AdminAuthenticationException extends  \Exception
{
}