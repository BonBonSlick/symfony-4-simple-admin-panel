<?php
declare(strict_types = 1);

namespace App\Domain\Core\Query;

class QueryPagination
{
    /**
     * @var int
     */
    private $page = 1;

    /**
     * @var int
     */
    private $perPage = 10;

    /**
     * Pagination constructor.
     *
     * @param int $page
     * @param int $perPage
     */
    public function __construct($page = 1, $perPage = 10)
    {
        $this->page = $page ?: $this->page;
        $this->perPage = $perPage ?: $this->perPage;
    }

    /**
     * @return int
     */
    public function page() : int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function offset() : int
    {
        return $this->perPage * ($this->page - 1);
    }

    /**
     * @return int
     */
    public function limit() : int
    {
        return $this->perPage;
    }
}