creates migrations from entities
- ```php bin/console doctrine:migrations:diff```

executes migrations to DB
- ```php bin/console doctrine:migrations:migrate```

loads seeds / fixtures
- ```php bin/console doctrine:fixtures:load```
