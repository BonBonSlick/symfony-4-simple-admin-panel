let Encore = require('@symfony/webpack-encore');
Encore
    .setOutputPath('public/')
    .setPublicPath('/')
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .addStyleEntry('css/framework', './node_modules/bootstrap/dist/css/bootstrap.css')
    .addStyleEntry('css/fonts', './node_modules/font-awesome/css/font-awesome.css')
    .enableSassLoader()
;
module.exports = Encore.getWebpackConfig();
